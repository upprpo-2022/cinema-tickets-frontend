import React from 'react';
import {useParams} from 'react-router-dom'
import UDUser from "../components/AdminAcc/Tables/Users/UDUser";

function AdminUserPage() {
    let params = useParams();
    return (
        <UDUser id={Number(params.id)}/>
    );
}

export default AdminUserPage;