import React from 'react';
import {useParams} from 'react-router-dom'
import UDTheater from "../components/AdminAcc/Tables/Theaters/UDTheater";

function AdminTheaterPage() {
    let params = useParams();
    return (
        <UDTheater id={Number(params.id)}/>
    );
}

export default AdminTheaterPage;