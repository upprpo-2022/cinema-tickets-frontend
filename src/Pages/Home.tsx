import React, {Component} from 'react'
import HomeCarouselBox from '../components/HomeCarouselBox'

export default class Home extends Component {
    render() {
        return (
                <HomeCarouselBox />
        )
    }
}