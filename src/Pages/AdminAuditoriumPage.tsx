import React from 'react';
import {useParams} from 'react-router-dom'
import UDAuditorium from "../components/AdminAcc/Tables/Auditoriums/UDAuditorium";

function AdminAuditoriumPage() {
    let params = useParams();
    return (
        <UDAuditorium id={Number(params.id)}/>
    );
}

export default AdminAuditoriumPage;