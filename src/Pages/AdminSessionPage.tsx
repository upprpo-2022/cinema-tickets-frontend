import React from 'react';
import {useParams} from 'react-router-dom'
import UDSession from "../components/AdminAcc/Tables/Sessions/UDSession";

function AdminSessionPage() {
    let params = useParams();
    return (
        <UDSession id={Number(params.id)}/>
    );
}

export default AdminSessionPage;