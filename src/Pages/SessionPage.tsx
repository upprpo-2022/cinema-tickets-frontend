import React from 'react';
import {useParams} from 'react-router-dom'
import CallSelectSeats from "../components/SelectSeats/CallSelectSeats";

function SessionPage() {
    let params = useParams();
    return (
        <CallSelectSeats id={Number(params.id)}/>
    );
}

export default SessionPage;