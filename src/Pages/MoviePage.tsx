import React from 'react';
import {useParams} from 'react-router-dom'
import GetDateValue from "../components/ScheduleScreen/GetDateValue";

function MoviePage() {
    let params = useParams();
    return (
        <GetDateValue id={Number(params.id)}/>
    );
}

export default MoviePage;