import React from 'react';
import {useParams} from 'react-router-dom'
import UDMovie from "../components/AdminAcc/Tables/Movies/UDMovie";

function AdminUserPage() {
    let params = useParams();
    return (
        <UDMovie id={Number(params.id)}/>
    );
}

export default AdminUserPage;