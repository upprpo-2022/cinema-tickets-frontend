import React, {Component} from 'react'
import {Col, Container, Nav, Row, Tab} from 'react-bootstrap'
import social from '../assets/Social.jpg'

export default class About extends Component {
    render() {
        return (
            <>
                <br></br>
                <Container>
                    <Tab.Container id={"ledt-tabs-example"} defaultActiveKey={"first"}>
                        <Row>
                            <Col sm={3}>
                                <Nav variant={"pills"} className={"flex-column mt-2"}>
                                    <Nav.Item>
                                        <Nav.Link eventKey={"first"}> Мы в соц сетях </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey={"second"}> Команда </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey={"third"}> Разработка </Nav.Link>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <Nav.Link eventKey={"fourth"}> Предложения и пожелания </Nav.Link>
                                    </Nav.Item>
                                </Nav>
                            </Col>
                            <Col sm={9}>
                                <Tab.Content>
                                    <Tab.Pane eventKey={"first"}>
                                        <img
                                            src={social}
                                        />
                                    </Tab.Pane>
                                    <Tab.Pane eventKey={"second"}>

                                    </Tab.Pane>
                                    <Tab.Pane eventKey={"third"}>

                                    </Tab.Pane>
                                    <Tab.Pane eventKey={"fourth"}>
                                        //добавить форму обратной связи
                                    </Tab.Pane>
                                </Tab.Content>

                            </Col>
                        </Row>
                    </Tab.Container>
                </Container>
            </>
        )
    }
}