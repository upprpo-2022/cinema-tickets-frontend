import React from 'react';
import './input.css'

const Input = (props: { setValue: (arg0: string) => void; value: string | number | readonly string[] | undefined; type: string | (string & {}) | undefined; placeholder: string | undefined; }) => {
    return (
        <div>
            <input onChange={(event)=> props.setValue(event.target.value)}
                   value={props.value}
                   type={props.type}
                   placeholder={props.placeholder}/>
        </div>
    );
};

export default Input;