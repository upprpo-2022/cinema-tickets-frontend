import React from 'react';
import {Session} from "../../Definitions";
import {Link} from "react-router-dom";

function TheaterSessions(props: { sessions: Session[], movieId: number }) {
    if (!props.sessions) {
        return (<div></div>)
    }

    let res = props.sessions;
    const session = res.map(function (item, index) {
        return <div key={index}>
            <Link
                to={"/cinema/movie" + props.movieId + "/session" + item.id}>{item.datetime.toString().substring(11, 16)}</Link>
        </div>
    })

    return (
        <div>
            {session}
        </div>
    );
}

export default TheaterSessions;
