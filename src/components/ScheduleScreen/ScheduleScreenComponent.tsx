import React from "react-query";
import {Movie} from "../../Definitions";
import {UseQueryResult} from "react-query/types/react/types";
import TheaterSessions from "./TheaterSessions";

function ScheduleScreenComponents(props: { sessionQuery: UseQueryResult<{ res: Movie }>, movieId: number }) {
    if (!props.sessionQuery.data) {
        return (<div></div>)
    }

    let res = props.sessionQuery.data.res;

    const theaters = res.theaters.map(function (item, index) {
        return <div key={index}>
            <span><h3>{item.name}:</h3></span>
            <br/>
            <span>{item.address}</span>:
            <span><TheaterSessions sessions={item.sessions} movieId={props.movieId}/></span>
        </div>
    })

    return (
        <div>
            {theaters}
        </div>
    );
}

export default ScheduleScreenComponents;
