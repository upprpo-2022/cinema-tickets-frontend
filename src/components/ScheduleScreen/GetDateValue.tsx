import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField'
import {useQuery, UseQueryResult} from "react-query";
import {Movie, Movies} from "../../Definitions";
import ScheduleScreenComponent from "./ScheduleScreenComponent";

export default function GetDateValue(props: { id: number }) {
    const [value, setValue] = useState("2022-05-03");

    let sessionQuery: UseQueryResult<{ res: Movie; }, unknown>;

    const handleChange = (e: { target: { value: React.SetStateAction<string>; }; }) => {
        setValue(e.target.value);
        sessionQuery.remove();
    }

    let startDate = value + "T01:01:01-00:00";
    let endDate = value + "T23:59:59-00:00";

    sessionQuery = useQuery<{ res: Movie }>('sessionQuery',
        () => fetch(process.env.REACT_APP_BACKEND_API_URL + '/movie-schedule?movie=' + props.id + '&startDate=' + startDate +
            '&endDate=' + endDate + '&page=1')
            .then(res => res.json())
            .then((res: Movie) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    const movieQuery = useQuery<{ res: Movies }>('movieQuery',
        () => fetch(process.env.REACT_APP_BACKEND_API_URL + '/movie/' + props.id)
            .then(res => res.json())
            .then((res: Movies) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    if (!movieQuery.data) {
        return (<div></div>)
    }

    let movie = movieQuery.data.res;

    return (
        <div>
            <div className={"GetDateValue"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Выберите дату для просмотра фильма</h3>
                <h3>"{movie.name}"</h3>
                <TextField
                    id="date"
                    type="date"
                    value={value}
                    onChange={handleChange}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <br></br>
                <br></br>
                <video width={"720"} height={"400"}
                       src={movie.previewUrl}
                       controls/>
            </div>
            <br></br>
            <div className={"ScheduleScreenComponent"} style={{
                paddingLeft: 30
            }}><ScheduleScreenComponent sessionQuery={sessionQuery} movieId={props.id}/></div>
        </div>
    )
}