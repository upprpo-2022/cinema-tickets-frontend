import React, {useState} from 'react';
import './registration.css'
import Input from "../../utils/input/Input";
import Modal from '../Modal/Modal'
import {useQuery} from "react-query";
import {ChangePasswordOrder} from "../../Definitions";

const PersonalAccount = () => {

    const userEmail = localStorage.getItem('email');
    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [successModalActive, setSuccessModalActive] = useState(false);
    const [unsuccessModalActive, setUnsuccessModalActive] = useState(false);

    const changePasswordQuery = useQuery<string>('changePasswordQuery',
        () => fetch(process.env.REACT_APP_BACKEND_BASE_URL + '/change-password',
            {
                method: 'POST',
                body: JSON.stringify({
                    username: userEmail,
                    oldPassword: oldPassword,
                    newPassword: newPassword
                } as ChangePasswordOrder),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    function changePass(password: any, repeatPassword: any) {
        
        if (password == repeatPassword) {
            changePasswordQuery.refetch().then(res => {
                console.log(res)
                if (res.data != "{\"errorString\":\"BAD_PASSWORD\",\"errorMessage\":\"Wrong password\"}" && res.data != "{\"errorString\":\"BAD_CREDENTIALS\",\"errorMessage\":null}") {
                    console.log("success")
                    setSuccessModalActive(true);
                } else {
                    console.log("unsucces")
                    setUnsuccessModalActive(true)
                }
            }, res => console.log(res))
            localStorage.setItem('password', password);
        } else {
            setUnsuccessModalActive(true);
        }
    }

    return (
        <div className='authorization'>
            <main>
                <div className="authorization__header">Личный кабинет</div>
                <div className="authorization__header">{userEmail}</div>
                <Input value={oldPassword} setValue={setOldPassword} type="password"
                       placeholder="Введите текущий пароль..."/>
                <Input value={newPassword} setValue={setNewPassword} type="password"
                       placeholder="Введите новый пароль..."/>
                <Input value={repeatPassword} setValue={setRepeatPassword} type="password"
                       placeholder="Повторите введеный пароль..."/>
                <button className="authorization__btn"
                        onClick={() => changePass(newPassword, repeatPassword)}>Сменить
                    пароль
                </button>
            </main>

            <Modal active={successModalActive} setActive={setSuccessModalActive}>
                <p> Пароль успешно изменен! </p>
            </Modal>

            <Modal active={unsuccessModalActive} setActive={setUnsuccessModalActive}>
                <p> Ошибка: пароли должны совпадать / текущий пароль неверен! </p>
            </Modal>
        </div>
    );
};

export default PersonalAccount;