import React, {useState} from 'react';
import './registration.css'
import Input from "../../utils/input/Input";
import {useNavigate} from 'react-router-dom'
import {setUser} from "../../reducers/userReducer"
import {useQuery} from "react-query";
import Modal from "../Modal/Modal";

const Login = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [unsuccessModalActive, setUnsuccessModalActive] = useState(false);

    const navigation = useNavigate();

    const loginQuery = useQuery<string>('loginQuery',
        () => fetch(process.env.REACT_APP_BACKEND_BASE_URL + '/login',
            {
                method: 'POST',
                body: "username=" + email + "&password=" + password,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/x-www-form-urlencoded',
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    function login(email: any, password: any) {
        loginQuery.refetch().then(res => {
            console.log(res);
            if (res.data === "{\"errorString\":\"BAD_CREDENTIALS\",\"errorMessage\":null}" || res.data === "{\"errorString\":\"BAD_PASSWORD\",\"errorMessage\":\"Wrong password\"}") {
                console.log("data " + res.data + " unsuccess");
                setUnsuccessModalActive(true)
            } else {
                if (res.data === '{"role":"ADMIN"}') {
                    setUser("ADMIN", email, password);
                } else if (res.data === '{"role":"USER"}') {
                    setUser("USER", email, password);
                }
                console.log("success ADMIN " + res.data);
                navigation("/");
                window.location.reload();
            }
        }, res => console.log(res))
    }

    return (
        <div className='authorization'>
            <main>
                <br></br>
                <div className="authorization__header">Авторизация</div>
                <Input value={email} setValue={setEmail} type="text" placeholder="Введите email..."/>
                <Input value={password} setValue={setPassword} type="password" placeholder="Введите пароль..."/>
                <button className="authorization__btn" onClick={() => login(email, password)}>Войти</button>
            </main>
            <Modal active={unsuccessModalActive} setActive={setUnsuccessModalActive}>
                <p> Ошибка: неправильно введен email или пароль </p>
            </Modal>
        </div>
    );
};

export default Login;