import React, {useState} from 'react';
import './registration.css'
import Input from "../../utils/input/Input"
import Modal from '../Modal/Modal'
import {useQuery} from "react-query";
import {RegisterOrder} from "../../Definitions";

const Registarion = () => {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [modalActive, setModalActive] = useState(false);
    const [unsuccessModalActive, setUnsuccessModalActive] = useState(false);


    const registerQuery = useQuery<string>('registerQuery',
        () => fetch(process.env.REACT_APP_BACKEND_BASE_URL + '/register',
            {
                method: 'POST',
                body: JSON.stringify({
                    username: email,
                    password: password
                } as RegisterOrder),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    function registration() {
        registerQuery.refetch().then(res => {
            console.log(res);
            if (res.data != "{\"errorString\":\"BAD_PASSWORD\",\"errorMessage\":\"Wrong password\"}" && res.data != "{\"errorString\":\"BAD_CREDENTIALS\",\"errorMessage\":null}") {
                console.log("success")
                setModalActive(true)
            } else {
                console.log("unsucces")
                setUnsuccessModalActive(true)
            }
        }, res => console.log(res))

        setModalActive(true);

    }

    return (
        <div>
            <div className='authorization'>
                <main>
                    <div className="authorization__header">Регистрация</div>
                    <Input value={email} setValue={setEmail} type="text" placeholder="Введите email..."/>
                    <Input value={password} setValue={setPassword} type="password" placeholder="Введите пароль..."/>
                    <button className="authorization__btn"
                            onClick={() => registration()}>Зарегистрироваться
                    </button>
                </main>
                <Modal active={modalActive} setActive={setModalActive}>
                    <p> Пользователь "{email}" успешно зарегистрирован </p>
                </Modal>

                <Modal active={unsuccessModalActive} setActive={setUnsuccessModalActive}>
                    <p> Ошибка: Пользователь "{email}" не был зарегистрирован </p>
                </Modal>
            </div>
        </div>
    );
};

export default Registarion;