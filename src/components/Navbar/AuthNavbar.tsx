import React from 'react';
import {Link, useNavigate} from "react-router-dom";
import {Button, Nav} from "react-bootstrap";
import {logout} from "../../reducers/userReducer";

const AuthNavbar = () => {

    let isAuth;
    const role = localStorage.getItem('role');
    let isAdmin = false;

    switch (role) {
        case "ADMIN":
            isAuth = true;
            isAdmin = true;
            break
        case "USER":
            isAuth = true;
            break
        case "UNAUTHORIZED_USER":
            isAuth = false;
            break
        default:
            isAuth = false;
    }

    console.log("Role " + role)

    const navigation = useNavigate();
    let logoutAcc = function () {
        logout();
        navigation("/");
        window.location.reload();
        console.log("Role in logout " + localStorage.getItem('role'));
    };

    return (
        <div>
            <Nav className={"me-sm-2"}>
                {!isAuth && <Nav.Link as={Link} to={'/login'}> Войти </Nav.Link>}
                {!isAuth && <Nav.Link as={Link} to={'/registration'}> Регистрация </Nav.Link>}
                {isAuth && isAdmin && <Nav.Link href={'/admin'}> Кабинет администратора </Nav.Link>}
                {isAuth && <Nav.Link as={Link} to={'/account'}> Личный кабинет </Nav.Link>}
                {isAuth && <Button onClick={(logoutAcc)}> Выход </Button>}
            </Nav>
        </div>
    );
};

export default AuthNavbar;