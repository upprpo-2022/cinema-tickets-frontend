import React, {useState} from 'react';
import SelectSeatsComponent from "./SelectSeatsComponent";

function CallSelectSeats(props: {id: number}) {
    const [isOpen, setIsOpen] = useState(true)

    return (
            <SelectSeatsComponent sessionId={props.id} isOpen={isOpen} setIsOpen={setIsOpen} />
    );
}

export default CallSelectSeats;