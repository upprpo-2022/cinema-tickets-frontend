import {Seat, SeatLayoutStatus} from "../../Definitions";
import {Button, Col, ListGroup, ListGroupItem, Row} from "react-bootstrap";
import {UseQueryResult} from "react-query/types/react/types";

function SelectedSeatsComponent(props: {setIsModalOpen: Function, ticketOrderQuery: UseQueryResult<string>, seatLayoutQuery: UseQueryResult<{res: SeatLayoutStatus, maxX: number, maxY: number}>, sessionId: number, selectedSeats: Seat[], setSelectedSeats: Function}) {
  function onClick() {
    props.ticketOrderQuery.refetch().then(res => { // TODO: handle error
      if (res.data === 'success') {
        props.setIsModalOpen(false)
      } else {
        console.log(res)
      }
    }, res => console.log(res))
  }

  return (
    <>
      <Row>
        <Col className='text-center'>
          Билеты
        </Col>
      </Row>
      <Row>
        <ListGroup className='px-0'>
          {
            props.selectedSeats.map((seat: Seat) => {
              return (
                <ListGroupItem key={seat.x.toString() + seat.y.toString()}>
                  <Row>
                    <Col className='text-start'>
                      Ряд {seat.y}, место {seat.x}
                    </Col>
                    <Col className='text-end'>
                      {seat.price} ₽
                    </Col>
                  </Row>
                </ListGroupItem>
              )
            })
          }
        </ListGroup>
      </Row>
      <Row className='d-flex flex-fill'>
        <Col className='d-flex flex-column justify-content-end'>
          <Row>
            {props.selectedSeats.length === 0 &&
            <Col className='text-center'>Выберите билеты для продолжения</Col>
            }
            {props.selectedSeats.length > 0 &&
            <Col>
              <Row>
                <Col className='text-start'>
                  Выбрано: {props.selectedSeats.length}
                </Col>
                <Col className='text-end'>
                  {props.selectedSeats.reduce((acc, seat) => acc + seat.price, 0)} ₽
                </Col>
              </Row>
            </Col>
            }
          </Row>
          <Row>
            <Button
              disabled={props.selectedSeats.length === 0 || props.ticketOrderQuery.isLoading || props.seatLayoutQuery.isLoading}
              onClick={() => onClick()} >Продолжить</Button>
          </Row>
        </Col>
      </Row>
    </>
  );
}

export default SelectedSeatsComponent;
