import {CloseButton, Container, Row} from "react-bootstrap";

function CustomModal(props: {children: any, isOpen: boolean, setIsOpen: Function}) {
  return (
    <>
      { props.isOpen && props.children &&
        <div
          style={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)'
          }}>
          <Container fluid>
            <Row className='justify-content-end border border-bottom-0'>
              <CloseButton onClick={() => props.setIsOpen(false)}/>
            </Row>
            <Row>
              {props.children}
            </Row>
          </Container>
        </div>
      }
    </>
  )
}

export default CustomModal;