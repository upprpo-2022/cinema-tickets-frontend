import {Layer, Stage} from "react-konva";
import SeatCircle from "./SeatCircle";
import {Seat, SeatLayoutStatus} from "../../Definitions";
import {UseQueryResult} from "react-query/types/react/types";

function SelectSeatsCanvas(props: {seatLayoutQuery: UseQueryResult<{res: SeatLayoutStatus, maxX: number, maxY: number}>, setSelectedSeats: Function, selectedSeats: Seat[]}) {
  if (!props.seatLayoutQuery.data) {
    return (<div> </div>)
  }

  let res = props.seatLayoutQuery.data.res
  let maxX = props.seatLayoutQuery.data.maxX
  let maxY = props.seatLayoutQuery.data.maxY

  let width = window.innerWidth * 0.7
  let height = window.innerHeight * 0.7

  let borderX = width / 10
  let borderY = height / 10 + 100

  let offsetX = (width - borderX) / maxX
  let offsetY = (height - borderY) / maxY

  offsetX = Math.min(offsetX, offsetY)
  offsetY = Math.min(offsetX, offsetY)

  return (
    <Stage
      width={width}
      height={height}>
      <Layer>
        {
          res.seats
            .sort((a: Seat, b: Seat) => a.y > b.y ? 1 : -1) // Workaround to make labels not overlap circles when rendering
            .map((seat: Seat) =>
            <SeatCircle
              setSelectedSeats={props.setSelectedSeats}
              selectedSeats={props.selectedSeats}
              circleRadius={Math.max(offsetX, offsetY) / 3}
              coordX={(seat.x - 1) * offsetX + width / 2 - (maxX - 1) * offsetX / 2}
              coordY={(seat.y - 1) * offsetY + height / 2 - (maxY - 1) * offsetY / 2}
              x={seat.x}
              y={seat.y}
              price={seat.price}
              status={seat.status}
              key={seat.x.toString() + seat.y.toString()} />
          )
        }
      </Layer>
    </Stage>
  );
}

export default SelectSeatsCanvas;
