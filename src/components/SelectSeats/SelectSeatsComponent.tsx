import {useQuery} from "react-query";
import SelectSeatsCanvas from "./SelectSeatsCanvas";
import {Seat, SeatLayoutStatus, TicketOrder} from "../../Definitions";
import {useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import SelectedSeatsComponent from "./SelectedSeatsComponent";
import CustomModal from "./CustomModal";

 function SelectSeatsComponent(props: {sessionId: number, isOpen: boolean, setIsOpen: Function}) {
  const [selectedSeats, setSelectedSeats] = useState<any[]>([])

  const seatLayoutStatusQuery = useQuery<{res: SeatLayoutStatus, maxX: number, maxY: number}>('seatLayoutStatusQuery' + props.sessionId,
    () => fetch(process.env.REACT_APP_BACKEND_API_URL + '/sessions/' + props.sessionId + '/seats')
      .then(res => res.json())
      .then((res: SeatLayoutStatus) => { // Add max x, max y coordinates to result
        let maxX = 0
        let maxY = 0

        res.seats.forEach((seat: Seat) => {
          if (seat.x > maxX) maxX = seat.x
          if (seat.y > maxY) maxY = seat.y
        })

        return {
          res: res,
          maxX: maxX,
          maxY: maxY
        }
      }),
    {
      staleTime: Infinity,
      cacheTime: 0
    }
  )

  const ticketOrderQuery = useQuery<string>('ticketOrderQuery', // TODO: queryKey = ???
    () => fetch(process.env.REACT_APP_BACKEND_API_URL + '/ticket-order',
      {
        method: 'POST',
        body: JSON.stringify({
          session: props.sessionId,
          seats: selectedSeats
        } as TicketOrder),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      }).then(res => res.text()),
    {
      staleTime: Infinity,
      cacheTime: 0,
      enabled: false
    }
  )

  return (
    <CustomModal isOpen={props.isOpen} setIsOpen={props.setIsOpen}>
      { !(seatLayoutStatusQuery.isLoading || seatLayoutStatusQuery.isError) &&
        <Container fluid className='border'>
          <Row>
            <Col>
              <SelectSeatsCanvas
                seatLayoutQuery={seatLayoutStatusQuery}
                setSelectedSeats={setSelectedSeats}
                selectedSeats={selectedSeats}/>
            </Col>
            <Col className='border-start d-flex flex-column' style={{width: '15vw'}}>
              <SelectedSeatsComponent
                setIsModalOpen={props.setIsOpen}
                ticketOrderQuery={ticketOrderQuery}
                seatLayoutQuery={seatLayoutStatusQuery}
                sessionId={props.sessionId}
                selectedSeats={selectedSeats}
                setSelectedSeats={setSelectedSeats}/>
            </Col>
          </Row>
        </Container>
      }
    </CustomModal>
  );
}

export default SelectSeatsComponent;
