import {Circle, Group, Label, Tag, Text} from "react-konva";
import React, {useEffect, useState} from "react";
import {Seat, Status} from "../../Definitions";
import Konva from "konva";

function SeatCircle(props: {setSelectedSeats: Function, selectedSeats: Seat[], circleRadius: number, coordX: number, x: number, y: number, coordY: number, price: number, status: Status}) {
  const [isMouseOver, setIsMouseOver] = useState<boolean>(false)
  const [isSelected, setIsSelected] = useState<boolean>(false)
  const [circleRef, setCircleRef] = useState<Konva.Circle>()
  const [labelRef, setLabelRef] = useState<Konva.Label>()

  useEffect(() => {
    if (isMouseOver) {
      labelRef?.to({
        opacity: 1,
        duration: 0.05
      })
    } else {
      labelRef?.to({
        opacity: 0,
        duration: 0.1
      })
    }

    if (props.status !== Status.FREE) {
      return
    }

    if (isSelected) {
      circleRef?.to({
        scaleX: 1.3,
        scaleY: 1.3,
        duration: 0.01
      })
    } else if (isMouseOver) {
      circleRef?.to({
        scaleX: 1.2,
        scaleY: 1.2,
        duration: 0.01
      })
    } else {
      circleRef?.to({
        scaleX: 1,
        scaleY: 1,
        duration: 0.01
      })
    }
  })

  useEffect(() => {
    setIsSelected(props.selectedSeats.findIndex((seat: Seat) => seat.x === props.x && seat.y === props.y) !== -1)
  })

  function onClick() {
    if (props.status === Status.FREE) {
      let newSelectedSeats = props.selectedSeats.slice()

      if (isSelected) {
        newSelectedSeats = newSelectedSeats.filter((seat: Seat) => !(seat.x === props.x && seat.y === props.y))
      } else {
        newSelectedSeats.push({
          x: props.x,
          y: props.y,
          price: props.price,
          status: props.status
        })
      }

      props.setSelectedSeats(newSelectedSeats)
    }
  }

  return (
    <Group
      x={props.coordX}
      y={props.coordY}
      onMouseLeave={() => setIsMouseOver(false)}
      onClick={onClick}>
      <Label
        ref={ref => { if (ref) setLabelRef(ref) }}
        opacity={isMouseOver ? 1 : 0}
        y={props.status === Status.FREE ? -props.circleRadius * 1.2 : -props.circleRadius / 2 * 1.2} >
        <Tag
          fill={'white'}
          cornerRadius={20}
          pointerDirection={'down'}
          pointerWidth={15}
          pointerHeight={15}
          shadowEnabled={true}
          shadowBlur={10}
          listening={false} />
        <Text
          text={props.status === Status.FREE ?
            props.price + ' ₽\nРяд ' + props.y + ' место ' + props.x :
            'Занято\nРяд ' + props.y + ' место ' + props.x}
          align={'center'}
          verticalAlign={'middle'}
          fill={'black'}
          fontSize={15}
          padding={12}
          listening={false} />
      </Label>
      <Circle
        ref={ref => { if (ref) setCircleRef(ref) }}
        radius={props.status === Status.FREE ? props.circleRadius : props.circleRadius / 2}
        fill={props.status === Status.FREE ? 'blue' : 'grey'}
        onMouseOver={() => setIsMouseOver(true)} />
      <Text
        text={props.x.toString()}
        visible={props.status === Status.FREE && (isSelected || isMouseOver)}
        x={-props.circleRadius}
        y={-props.circleRadius + props.circleRadius * 0.1} // "+ props.circleRadius * 0.1" is a workaround for fonts that are not vertically centered
        width={props.circleRadius * 2}
        height={props.circleRadius * 2}
        align={'center'}
        verticalAlign={'middle'}
        fill={'white'}
        fontSize={props.circleRadius * 1.4}
        listening={false} />
    </Group>
  );
}

export default SeatCircle;
