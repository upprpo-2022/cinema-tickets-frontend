import React from "react-query";
import {MovieOrder} from "../../Definitions";
import {UseQueryResult} from "react-query/types/react/types";
import {Link} from "react-router-dom";

function MovieComponent(props: { movieQuery: UseQueryResult<{ res: MovieOrder }> }) {
    if (!props.movieQuery.data) {
        return (<div></div>)
    }

    let res = props.movieQuery.data.res;
    const movies = res.movies.map(function (item, index) {
        return <div key={index}>
            <h3><Link to={"/cinema/movie" + item.id}>{item.name}</Link></h3>
            <br></br>
            <span>{item.description}</span>
            <br></br>
            <video width={"720"} height={"400"}
                   src={item.previewUrl}
                   controls/>
        </div>
    })

    return (
        <div>
            {movies}
        </div>
    );
}

export default MovieComponent;
