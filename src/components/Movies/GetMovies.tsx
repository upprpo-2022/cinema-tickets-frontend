import React from 'react';
import {useQuery} from "react-query";
import {MovieOrder} from "../../Definitions";
import MovieComponent from "./MovieComponent";

export default function GetMovie() {

    const movieQuery = useQuery<{ res: MovieOrder }>('movieQuery',
        () => fetch(process.env.REACT_APP_BACKEND_API_URL + '/movies?page=1')
            .then(res => res.json())
            .then((res: MovieOrder) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    return (
        <div>
            <div className={"GetMovies"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Выберите фильм, который хотели бы посмотреть</h3>
            </div>

            <div className={"MovieComponent"} style={{
                paddingLeft: 30
            }}><MovieComponent movieQuery={movieQuery}/></div>
        </div>
    )
}