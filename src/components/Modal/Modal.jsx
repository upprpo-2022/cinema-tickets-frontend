import React from 'react'
import './modalScreen.css'

const Modal = ({active, setActive, children}) => {
    return (
        <div className={active ? "modal-screen active" : "modal-screen"} onClick={() => setActive(false)}>
            <div className={"modal-screen__content"} onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
}

export default Modal;