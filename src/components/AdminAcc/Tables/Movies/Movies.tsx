import React, {useState} from 'react';
import Input from "../../../../utils/input/Input";
import {Button} from "react-bootstrap";
import {useQuery} from "react-query";
import {Movies as Movie} from "../../../../Definitions";
import {useNavigate} from "react-router-dom";

const Movies = () => {

    const [id, setId] = useState("")
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")

    const navigation = useNavigate();

    const createMovieQuery = useQuery<string>('createMovieQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/movies',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: id,
                    name: name,
                    description: description
                } as unknown as Movie),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    let addMovie = function () {
        createMovieQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    };

    return (
        <div>
            <h2>Добавить новый фильм</h2>
            <Input value={id} setValue={setId} type="text" placeholder="Введите id"/>
            <Input value={description} setValue={setDescription} type="text" placeholder="Введите описание фильма"/>
            <Input value={name} setValue={setName} type="text" placeholder="Введите название фильма"/>
            <br></br>
            <Button onClick={() => addMovie()}>Добавить фильм</Button>
        </div>
    );
};

export default Movies;