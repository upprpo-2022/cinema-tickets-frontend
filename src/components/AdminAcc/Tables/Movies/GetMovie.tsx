import React from 'react';
import {useQuery} from "react-query";
import {AdminMovieOrder} from "../../../../Definitions";
import {MDBTable, MDBTableBody, MDBTableHead} from 'mdbreact';
import {Link} from "react-router-dom";

export default function GetMovie() {

    let adminMovieQuery = useQuery<{ res: AdminMovieOrder }>('adminMovieQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/movies?page=1', {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminMovieOrder) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    if (!adminMovieQuery.data) {
        return (<div></div>)
    }

    let res = adminMovieQuery.data.res;

    return (
        <div>
            <div className={"GetUser"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Список зарегистрированных пользователей</h3>
            </div>

            <MDBTable>
                <MDBTableHead>
                    <tr>
                        <th>ID</th>
                        <th>Название фильма</th>
                        <th>Описание</th>
                    </tr>
                </MDBTableHead>
                <MDBTableBody>
                    {
                        res.list.map((item, index) =>
                            <tr key={index}>
                                <td><Link to={"/admin/movie" + item.id}>{item.id}</Link></td>
                                <td> {item.name} </td>
                                <td> {item.description} </td>
                            </tr>
                        )
                    }
                </MDBTableBody>
            </MDBTable>

        </div>
    )
}