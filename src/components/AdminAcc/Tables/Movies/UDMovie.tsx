import React, {useState} from 'react';
import {useQuery} from "react-query";
import {Movies} from "../../../../Definitions";
import Input from "../../../../utils/input/Input";
import '../../../Authorization/registration.css'
import {useNavigate} from "react-router-dom";

export default function UDMovie(props: { id: number }) {

    const [movieName, setMovieName] = useState("");
    const [movieDescription, setMovieDescription] = useState("");

    const navigation = useNavigate();
    let trailerUrl = "";

    const movieQuery = useQuery<{ res: Movies }>('movieQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/movies/' + props.id, {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: Movies) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    const postMovieQuery = useQuery<string>('postMovieQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/movies',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: props.id,
                    name: movieName,
                    description: movieDescription,
                    previewUrl: trailerUrl
                } as Movies),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    const deleteMovieQuery = useQuery<string>('deleteMovieQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/movies/' + props.id,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    if (!movieQuery.data) {
        return (<div></div>)
    }

    let movie = movieQuery.data.res;

    trailerUrl = movie.previewUrl;

    function update() {
        postMovieQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function deleteMovie() {
        deleteMovieQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function applyDefaultValue() {
        setMovieName(movie.name);
        setMovieDescription(movie.description);
    }

    return (
        <div>
            <div className={"UDMovie"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Обновление фильма</h3>

                <h3>{movie.name}</h3>
            </div>
            <div className='authorization'>
                <main>
                    <div className="authorization__header">Обновление</div>
                    <Input value={movieName} setValue={setMovieName} type="text" placeholder={movie.name}/>
                    <Input value={movieDescription} setValue={setMovieDescription} type="text"
                           placeholder={movie.description}/>
                    <button className="authorization__btn"
                            onClick={() => update()}>Обновить информацию о фильме
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => deleteMovie()}>Удалить фильм
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => applyDefaultValue()}>Применить значения
                    </button>

                    <h3>Загрузить новый трейлер</h3>

                    <form
                        action={process.env.REACT_APP_ADMIN_API_URL + '/upload-movie-preview?id=' + props.id}
                        method="post"
                        encType="multipart/form-data">
                        <input name="file" type="file"/>
                        <button className="authorization__btn" type="submit"> Загрузить трейлер </button>
                    </form>
                </main>
            </div>
        </div>
    )
}