import React, {useState} from 'react';
import Input from "../../../../utils/input/Input";
import {Button} from "react-bootstrap";
import {useQuery} from "react-query";
import {AdminTheater} from "../../../../Definitions";
import {useNavigate} from "react-router-dom";

const Theater = () => {

    const [id, setId] = useState("")
    const [address, setAddress] = useState("")
    const [name, setName] = useState("")
    const navigation = useNavigate();

    const createTheaterQuery = useQuery<string>('createTheaterQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/theaters',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: id,
                    name: name,
                    address: address
                } as unknown as AdminTheater),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    let addTheater = function () {
        createTheaterQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    };

    return (
        <div>
            <h2>Добавить новый кинотеатр</h2>
            <Input value={id} setValue={setId} type="text" placeholder="Введите id"/>
            <Input value={address} setValue={setAddress} type="text" placeholder="Введите адрес"/>
            <Input value={name} setValue={setName} type="text" placeholder="Введите название кинотеатра"/>
            <br></br>
            <Button onClick={() => addTheater()}>Добавить кинотеатр</Button>
        </div>
    );
};

export default Theater;