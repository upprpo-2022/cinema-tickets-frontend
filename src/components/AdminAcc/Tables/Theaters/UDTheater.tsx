import React, {useState} from 'react';
import {useQuery} from "react-query";
import {AdminTheater} from "../../../../Definitions";
import Input from "../../../../utils/input/Input";
import '../../../Authorization/registration.css'
import {useNavigate} from "react-router-dom";

export default function UDAuditorium(props: { id: number }) {

    const [name, setName] = useState("");
    const [address, setAddress] = useState("");

    const navigation = useNavigate();

    const theaterQuery = useQuery<{ res: AdminTheater }>('theaterQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/theaters/' + props.id, {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminTheater) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    const postTheaterQuery = useQuery<string>('postTheaterQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/theaters',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: props.id,
                    name: name,
                    address: address
                } as unknown as AdminTheater),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    const deleteTheaterQuery = useQuery<string>('deleteTheaterQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/theaters/' + props.id,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    if (!theaterQuery.data) {
        return (<div></div>)
    }

    let theater = theaterQuery.data.res;

    function update() {
        postTheaterQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function deleteTheater() {
        deleteTheaterQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function applyDefaultValue() {
        setName(theater.name);
        setAddress(theater.address);
    }

    return (
        <div>
            <div className={"UDAuditorium"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Обновление кинотеатра</h3>

                <h3>{theater.name}</h3>
            </div>
            <div className='authorization'>
                <main>
                    <div className="authorization__header">Обновление</div>
                    <Input value={name} setValue={setName} type="text" placeholder={theater.name}/>
                    <Input value={address} setValue={setAddress} type="text" placeholder={theater.address}/>
                    <button className="authorization__btn"
                            onClick={() => update()}>Обновить информацию об кинотеатре
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => deleteTheater()}>Удалить кинотеатр
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => applyDefaultValue()}>Применить значения
                    </button>
                </main>
            </div>
        </div>
    )
}