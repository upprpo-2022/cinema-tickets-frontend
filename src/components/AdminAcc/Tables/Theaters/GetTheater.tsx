import React from 'react';
import {useQuery} from "react-query";
import {AdminTheaterOrder} from "../../../../Definitions";
import {MDBTable, MDBTableBody, MDBTableHead} from 'mdbreact';
import {Link} from "react-router-dom";

export default function GetAuditorium() {

    let adminTheaterQuery = useQuery<{ res: AdminTheaterOrder }>('adminTheaterQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/theaters?page=1', {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminTheaterOrder) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    if (!adminTheaterQuery.data) {
        return (<div></div>)
    }

    let res = adminTheaterQuery.data.res;

    return (
        <div>
            <div className={"GetTheater"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Список кинотеатров</h3>
            </div>

            <MDBTable>
                <MDBTableHead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Аддрес</th>
                    </tr>
                </MDBTableHead>
                <MDBTableBody>
                    {
                        res.list.map((item, index) =>
                            <tr key={index}>
                                <td><Link to={"/admin/theater" + item.id}>{item.id}</Link></td>
                                <td> {item.name} </td>
                                <td> {item.address} </td>
                            </tr>
                        )
                    }
                </MDBTableBody>
            </MDBTable>

        </div>
    )
}