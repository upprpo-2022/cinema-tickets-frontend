import React from 'react';
import {useQuery} from "react-query";
import {AdminUserOrder} from "../../../../Definitions";
import {MDBTable, MDBTableBody, MDBTableHead} from 'mdbreact';
import {Link} from "react-router-dom";

export default function GetUser() {

    let userQuery = useQuery<{ res: AdminUserOrder }>('userQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/users?page=1', {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminUserOrder) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    if (!userQuery.data) {
        return (<div></div>)
    }

    let res = userQuery.data.res;

    return (
        <div>
            <div className={"GetUser"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Список зарегистрированных пользователей</h3>
            </div>

            <MDBTable>
                <MDBTableHead>
                    <tr>
                        <th>ID</th>
                        <th>Имя пользователя</th>
                        <th>Роль</th>
                    </tr>
                </MDBTableHead>
                <MDBTableBody>
                    {
                        res.list.map((item, index) =>
                            <tr key={index}>
                                <td><Link to={"/admin/user" + item.id}>{item.id}</Link></td>
                                <td> {item.username} </td>
                                <td> {item.role} </td>
                            </tr>
                        )
                    }
                </MDBTableBody>
            </MDBTable>

        </div>
    )
}