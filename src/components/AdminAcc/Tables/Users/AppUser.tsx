import React, {useState} from 'react';
import Input from "../../../../utils/input/Input";
import {Button} from "react-bootstrap";

const AppUser = () => {

    const [id, setId] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [role, setRole] = useState("")

    let addUser = function (id: any, username: any, password: any, role: any) {
        console.log(id);
        console.log(username);
        console.log(password);
        console.log(role);
    };

    return (
        <div>
            <h2>Добавить нового пользователя</h2>
            <Input value={id} setValue={setId} type="text" placeholder="Введите id"/>
            <Input value={username} setValue={setUsername} type="text" placeholder="Введите почту"/>
            <Input value={password} setValue={setPassword} type="password" placeholder="Введите пароль пользователя"/>
            <Input value={role} setValue={setRole} type="text" placeholder="Введите роль пользователя"/>
            <br></br>
            <Button onClick={() => addUser(id, username, password, role)}>Добавить пользователя</Button>
        </div>
    );
};

export default AppUser;