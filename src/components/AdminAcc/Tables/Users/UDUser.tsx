import React, {useState} from 'react';
import {useQuery} from "react-query";
import {User} from "../../../../Definitions";
import Input from "../../../../utils/input/Input";
import '../../../Authorization/registration.css'
import {useNavigate} from "react-router-dom";

export default function UDUser(props: { id: number }) {

    const [email, setEmail] = useState("");
    const [userPassword, setUserPassword] = useState("");
    const [userRole, setUserRole] = useState("");

    const navigation = useNavigate();

    const userQuery = useQuery<{ res: User }>('userQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/users/' + props.id, {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: User) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    const postUserQuery = useQuery<string>('postUserQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/users',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: props.id,
                    username: email,
                    password: userPassword,
                    role: userRole
                } as User),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    const deleteUserQuery = useQuery<string>('deleteUserQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/users/' + props.id,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    if (!userQuery.data) {
        return (<div></div>)
    }

    let user = userQuery.data.res;

    function update(password: any) {
        setUserPassword(password);

        postUserQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function deleteUser() {
        deleteUserQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function applyDefaultValue() {
        setEmail(user.username);
        setUserRole(user.role);
    }

    return (
        <div>
            <div className={"UDUser"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Обновление пользователя</h3>

                <h3>{user.username}</h3>
            </div>
            <div className='authorization'>
                <main>
                    <div className="authorization__header">Обновление</div>
                    <Input value={email} setValue={setEmail} type="text" placeholder={user.username}/>
                    <Input value={userRole} setValue={setUserRole} type="text" placeholder={user.role}/>
                    <button className="authorization__btn"
                            onClick={() => update(user.password)}>Обновить информацию о пользователе
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => deleteUser()}>Удалить пользователя
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => applyDefaultValue()}>Применить значения
                    </button>
                </main>
            </div>
        </div>
    )
}