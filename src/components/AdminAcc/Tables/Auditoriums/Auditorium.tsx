import React, {useState} from 'react';
import Input from "../../../../utils/input/Input";
import {Button} from "react-bootstrap";
import {useQuery} from "react-query";
import {Auditorium as Aud} from "../../../../Definitions";
import {useNavigate} from "react-router-dom";

const Auditorium = () => {

    const [id, setId] = useState("")
    const [seats, setSeats] = useState("[{\"x\":,\"y\":,\"price\":},{\"x\":,\"y\":,\"price\":},{\"x\":,\"y\":,\"price\":},{\"x\":,\"y\":,\"price\":},{\"x\":,\"y\":,\"price\":},{\"x\":,\"y\":,\"price\":}]")
    const [theaterId, setTheaterId] = useState("")
    const navigation = useNavigate();
    let seatJSON: any;

    const createAudQuery = useQuery<string>('createAudQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/auditoriums',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: id,
                    seats: seatJSON,
                    theaterId: theaterId
                } as unknown as Aud),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    let addAud = function () {
        seatJSON = JSON.parse(seats)

        createAudQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    };

    return (
        <div>
            <h2>Добавить новую аудиторию</h2>
            <Input value={id} setValue={setId} type="text" placeholder="Введите id"/>
            <Input value={seats} setValue={setSeats} type="text" placeholder="Введите места"/>
            <Input value={theaterId} setValue={setTheaterId} type="text" placeholder="Введите id кинотеатра"/>
            <br></br>
            <Button onClick={() => addAud()}>Добавить аудиторию</Button>
        </div>
    );
};

export default Auditorium;