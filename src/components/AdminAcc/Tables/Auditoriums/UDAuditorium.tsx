import React, {useState} from 'react';
import {useQuery} from "react-query";
import {Auditorium} from "../../../../Definitions";
import Input from "../../../../utils/input/Input";
import '../../../Authorization/registration.css'
import {useNavigate} from "react-router-dom";

export default function UDAuditorium(props: { id: number }) {

    const [stringSeats, setStringSeats] = useState("");
    const [theaterId, setTheaterId] = useState("");
    let seatJSON: any;

    const navigation = useNavigate();

    const auditoriumQuery = useQuery<{ res: Auditorium }>('auditoriumQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/auditoriums/' + props.id, {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: Auditorium) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    const postAudQuery = useQuery<string>('postAudQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/auditoriums',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: props.id,
                    seats: seatJSON,
                    theaterId: theaterId
                } as unknown as Auditorium),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    const deleteAudQuery = useQuery<string>('deleteAudQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/auditoriums/' + props.id,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    if (!auditoriumQuery.data) {
        return (<div></div>)
    }

    let auditorium = auditoriumQuery.data.res;

    function update() {
        seatJSON = JSON.parse(stringSeats)

        postAudQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function deleteAud() {
        deleteAudQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function applyDefaultValue() {
        setTheaterId(String(auditorium.theaterId));
        setStringSeats(JSON.stringify(auditorium.seats));
    }

    return (
        <div>
            <div className={"UDAuditorium"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Обновление аудитории</h3>

                <h3>{auditorium.id}</h3>
            </div>
            <div className='authorization'>
                <main>
                    <div className="authorization__header">Обновление</div>
                    <Input value={stringSeats} setValue={setStringSeats} type="text" placeholder={JSON.stringify(auditorium.seats)}/>
                    <Input value={theaterId} setValue={setTheaterId} type="text" placeholder={String(auditorium.theaterId)}/>
                    <button className="authorization__btn"
                            onClick={() => update()}>Обновить информацию об аудитории
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => deleteAud()}>Удалить аудиторию
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => applyDefaultValue()}>Применить значения
                    </button>
                </main>
            </div>
        </div>
    )
}