import React from 'react';
import {useQuery} from "react-query";
import {AdminAuditOrder} from "../../../../Definitions";
import {MDBTable, MDBTableBody, MDBTableHead} from 'mdbreact';
import {Link} from "react-router-dom";
import GetSeats from "./GetSeats"

export default function GetAuditorium() {

    let adminAuditoriumQuery = useQuery<{ res: AdminAuditOrder }>('adminAuditoriumQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/auditoriums?page=1', {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminAuditOrder) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    if (!adminAuditoriumQuery.data) {
        return (<div></div>)
    }

    let res = adminAuditoriumQuery.data.res;

    return (
        <div>
            <div className={"GetAud"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Список аудиторий</h3>
            </div>

            <MDBTable>
                <MDBTableHead>
                    <tr>
                        <th>ID</th>
                        <th>Места</th>
                        <th>ID кинотеатра</th>
                    </tr>
                </MDBTableHead>
                <MDBTableBody>
                    {
                        res.list.map((item, index) =>
                            <tr key={index}>
                                <td><Link to={"/admin/auditorium" + item.id}>{item.id}</Link></td>
                                <td> <GetSeats seats={item.seats} movieId={1}/> </td>
                                <td> {item.theaterId} </td>
                            </tr>
                        )
                    }
                </MDBTableBody>
            </MDBTable>

        </div>
    )
}