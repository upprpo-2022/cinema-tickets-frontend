import React, {useState} from 'react';
import {useQuery} from "react-query";
import {AdminSession} from "../../../../Definitions";
import Input from "../../../../utils/input/Input";
import '../../../Authorization/registration.css'
import {useNavigate} from "react-router-dom";

export default function UDAuditorium(props: { id: number }) {

    const [theaterId, setTheaterId] = useState("");
    const [movieId, setMovieId] = useState("");
    const [auditoriumId, setAuditoriumId] = useState("");
    const [stringSeats, setStringSeats] = useState("");
    const [dateTime, setDateTime] = useState("");

    let seatJSON: any;

    const navigation = useNavigate();

    const sessionQuery = useQuery<{ res: AdminSession }>('sessionQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/sessions/' + props.id, {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminSession) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    const postSessionQuery = useQuery<string>('postSessionQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/sessions',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: props.id,
                    theaterId: theaterId,
                    movieId: movieId,
                    auditoriumId: auditoriumId,
                    seats: seatJSON,
                    dateTime: dateTime
                } as unknown as AdminSession),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    const deleteSessionQuery = useQuery<string>('deleteSessionQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/sessions/' + props.id,
            {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    if (!sessionQuery.data) {
        return (<div></div>)
    }

    let session = sessionQuery.data.res;

    function update() {
        seatJSON = JSON.parse(stringSeats)

        postSessionQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function deleteSession() {
        deleteSessionQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    }

    function applyDefaultValue() {
        setTheaterId(String(session.theaterId));
        setMovieId(String(session.movieId));
        setAuditoriumId(String(session.auditoriumId));
        setStringSeats(JSON.stringify(session.seats));
        setDateTime(String(session.dateTime));
    }

    return (
        <div>
            <div className={"UDSession"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Обновление Сеанса</h3>

                <h3>{session.id}</h3>
            </div>
            <div className='authorization'>
                <main>
                    <div className="authorization__header">Обновление</div>
                    <Input value={theaterId} setValue={setTheaterId} type="text" placeholder="Введите id кинотеатра"/>
                    <Input value={movieId} setValue={setMovieId} type="text" placeholder="Введите id фильма"/>
                    <Input value={auditoriumId} setValue={setAuditoriumId} type="text" placeholder="Введите id аудитории"/>
                    <Input value={stringSeats} setValue={setStringSeats} type="text" placeholder={JSON.stringify(session.seats)}/>
                    <Input value={dateTime} setValue={setDateTime} type="text" placeholder={String(session.dateTime)}/>
                    <button className="authorization__btn"
                            onClick={() => update()}>Обновить информацию о сеансе
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => deleteSession()}>Удалить сеанс
                    </button>
                    {' '}
                    <button className="authorization__btn"
                            onClick={() => applyDefaultValue()}>Применить значения
                    </button>
                </main>
            </div>
        </div>
    )
}