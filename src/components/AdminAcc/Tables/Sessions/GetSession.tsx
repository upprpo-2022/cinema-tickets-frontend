import React from 'react';
import {useQuery} from "react-query";
import {AdminSessionOrder} from "../../../../Definitions";
import {MDBTable, MDBTableBody, MDBTableHead} from 'mdbreact';
import {Link} from "react-router-dom";
import GetSeats from "./GetSeats"

export default function GetSession() {

    let adminSessionQuery = useQuery<{ res: AdminSessionOrder }>('adminSessionQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/sessions?page=1', {
            method: 'GET',
            credentials: 'include'
        })
            .then(res => res.json())
            .then((res: AdminSessionOrder) => {

                return {
                    res: res,
                }

            }),
        {
            staleTime: Infinity,
            cacheTime: 0,
            keepPreviousData: false
        }
    )

    if (!adminSessionQuery.data) {
        return (<div></div>)
    }

    let res = adminSessionQuery.data.res;

    return (
        <div>
            <div className={"GetSession"} style={{
                margin: 'auto',
                display: 'block',
                width: 'fit-content'
            }}>
                <br></br>
                <h3>Список аудиторий</h3>
            </div>

            <MDBTable>
                <MDBTableHead>
                    <tr>
                        <th>ID</th>
                        <th>ID кинотеатра</th>
                        <th>ID фильма</th>
                        <th>ID аудитории</th>
                        <th>Места</th>
                        <th>Дата</th>
                    </tr>
                </MDBTableHead>
                <MDBTableBody>
                    {
                        res.list.map((item, index) =>
                            <tr key={index}>
                                <td><Link to={"/admin/session" + item.id}>{item.id}</Link></td>
                                <td> {item.theaterId} </td>
                                <td> {item.movieId} </td>
                                <td> {item.auditoriumId} </td>
                                <td> <GetSeats seats={item.seats} movieId={1}/> </td>
                                <td> {String(item.dateTime)} </td>
                            </tr>
                        )
                    }
                </MDBTableBody>
            </MDBTable>

        </div>
    )
}