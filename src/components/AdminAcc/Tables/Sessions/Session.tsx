import React, {useState} from 'react';
import Input from "../../../../utils/input/Input";
import {Button} from "react-bootstrap";
import {useQuery} from "react-query";
import {AdminSession} from "../../../../Definitions";
import {useNavigate} from "react-router-dom";

const Session = () => {

    const [id, setId] = useState("")
    const [date, setDate] = useState("2022-05-20T12:30:40-02:00")
    const [seats, setSeats] = useState("[{\"x\":1,\"y\":1,\"price\":450,\"status\":\"occupied\"},{\"x\":2,\"y\":1,\"price\":450,\"status\":\"occupied\"},{\"x\":1,\"y\":2,\"price\":450,\"status\":\"free\"},{\"x\":2,\"y\":2,\"price\":450,\"status\":\"occupied\"},{\"x\":1,\"y\":3,\"price\":450,\"status\":\"free\"},{\"x\":2,\"y\":3,\"price\":450,\"status\":\"free\"},{\"x\":1,\"y\":4,\"price\":450,\"status\":\"free\"},{\"x\":2,\"y\":4,\"price\":450,\"status\":\"free\"}]")
    const [audId, setAudId] = useState("")
    const [movieId, setMovieId] = useState("")
    const [theaterId, setTheaterId] = useState("")
    const navigation = useNavigate();
    let seatJSON: any;

    const createSessionQuery = useQuery<string>('createSessionQuery',
        () => fetch(process.env.REACT_APP_ADMIN_API_URL + '/sessions',
            {
                method: 'POST',
                body: JSON.stringify({
                    id: id,
                    theaterId: theaterId,
                    movieId: movieId,
                    auditoriumId: audId,
                    seats: seatJSON,
                    dateTime: date
                } as unknown as AdminSession),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.text()),
        {
            staleTime: Infinity,
            cacheTime: 0,
            enabled: false
        }
    );

    let addSession = function () {
        seatJSON = JSON.parse(seats)

        createSessionQuery.refetch().then(res => {
            console.log(res);
        }, res => console.log(res))

        navigation("/");
    };

    return (
        <div>
            <h2>Добавить новый сеанс</h2>
            <Input value={id} setValue={setId} type="text" placeholder="Введите id"/>
            <Input value={theaterId} setValue={setTheaterId} type="text" placeholder="Введите id кинотеатра"/>
            <Input value={movieId} setValue={setMovieId} type="text" placeholder="Введите id фильма"/>
            <Input value={audId} setValue={setAudId} type="text" placeholder="Введите id аудитории"/>
            <Input value={seats} setValue={setSeats} type="text" placeholder="Введите места"/>
            <Input value={date} setValue={setDate} type="text" placeholder="2022-05-20T12:30:40-02:00"/>
            <br></br>
            <Button onClick={() => addSession()}>Добавить сеанс</Button>
        </div>
    );
};

export default Session;