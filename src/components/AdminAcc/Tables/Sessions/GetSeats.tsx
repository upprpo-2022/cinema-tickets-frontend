import React from 'react';
import {Seat} from "../../../../Definitions";

function GetSeats(props: { seats: Seat[], movieId: number }) {
    if (!props.seats) {
        return (<div></div>)
    }

    let res = props.seats;
    const seats = res.map(function (item, index) {
        return <div key={index}>
            "x": {item.x}, "y": {item.y}, "price": {item.price}, "status": {item.status}
        </div>
    })

    return (
        <div>
            {seats}
        </div>
    );
}

export default GetSeats;
