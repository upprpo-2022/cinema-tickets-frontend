import React from 'react';
import {Col, Container, Nav, Row, Tab} from "react-bootstrap";
import Auditorium from './Tables/Auditoriums/Auditorium'
import Movies from './Tables/Movies/Movies'
import GetMovie from './Tables/Movies/GetMovie'
import Session from './Tables/Sessions/Session'
import Theater from './Tables/Theaters/Theater'
import AppUser from './Tables/Users/AppUser'
import GetUser from './Tables/Users/GetUser'
import GetAuditorium from './Tables/Auditoriums/GetAuditorium'
import GetSession from './Tables/Sessions/GetSession'
import GetTheater from './Tables/Theaters/GetTheater'

const Admin = () => {

    return (
        <div>
            <br></br>
            <Container>
                <Tab.Container id={"ledt-tabs-example"} defaultActiveKey={"first"}>
                    <Row>
                        <Col sm={3}>
                            <Nav variant={"pills"} className={"flex-column mt-2"}>
                                <Nav.Item>
                                    <Nav.Link eventKey={"first"}> Аудитории </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"second"}> Все аудитории </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"third"}> Фильмы </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"fourth"}> Все фильмы </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"fifth"}> Сеансы </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"sixth"}> Все сеансы </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"seventh"}> Кинотеатры </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"eighth"}> Все кинотеатры </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"ninth"}> Пользователи </Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey={"tenth"}> Все пользователи </Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Col>
                        <Col sm={9}>
                            <Tab.Content>
                                <Tab.Pane eventKey={"first"}>
                                    <Auditorium/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"second"}>
                                    <GetAuditorium/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"third"}>
                                    <Movies/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"fourth"}>
                                    <GetMovie/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"fifth"}>
                                    <Session/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"sixth"}>
                                    <GetSession/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"seventh"}>
                                    <Theater/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"eighth"}>
                                    <GetTheater/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"ninth"}>
                                    <AppUser/>
                                </Tab.Pane>
                                <Tab.Pane eventKey={"tenth"}>
                                    <GetUser/>
                                </Tab.Pane>
                            </Tab.Content>

                        </Col>
                    </Row>
                </Tab.Container>
            </Container>
        </div>
    );
};

export default Admin;