import React, {Component} from 'react';
import Carousel from 'react-bootstrap/Carousel'
import filmStrip from '../assets/P1.jpg'
import cinema from '../assets/P2.jpg'
import popcorn from '../assets/P3.jpg'

class HomeCarouselBox extends Component {
    render() {
        return (
            <Carousel>
                <Carousel.Item>
                    <img
                        className={"d-block w-100"}
                        src={filmStrip}
                        alt={"FilmStrip"}
                    />
                    <Carousel.Caption>
                        <h3>Все кинотеатры Новосибирска</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className={"d-block w-100"}
                        src={cinema}
                        alt={"Cinema"}
                    />
                    <Carousel.Caption>
                        <h3>Развлекайся с друзьями и близкими</h3>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className={"d-block w-100"}
                        src={popcorn}
                        alt={"Popcorn"}
                    />
                    <Carousel.Caption>
                        <h3>Вкусный отдых</h3>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        );
    }
}

export default HomeCarouselBox;