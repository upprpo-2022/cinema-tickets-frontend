import React, {Component} from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap';
import logo from './Logo.png';
import {Link} from "react-router-dom";
import AuthNavbar from '../components/Navbar/AuthNavbar'

export default class Header extends Component {
    render() {
        return (
            <>
                <br></br>
                <br></br>
                <Navbar fixed={"top"} collapseOnSelect expand={"md"} bg={"primary"} variant={"light"}>
                    <Container>
                        <Navbar.Brand href="/">
                            <img
                                src={logo}
                                height={"30"}
                                width={"30"}
                                className={"d-inline-block align-top"}
                                alt={"Logo"}
                            /> АФИША
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls={"responsive-navbar-nav"}/>
                        <Navbar.Collapse id={"responsive-navbar-nav"}>
                            <Nav className={"me-auto"}>
                                <Nav.Link as={Link} to={'/'}> Все </Nav.Link>
                                <Nav.Link as={Link} to={'/cinema'}> Фильмы </Nav.Link>
                                <Nav.Link as={Link} to={'/contacts'}> Контакты </Nav.Link>
                                <Nav.Link as={Link} to={'/about'}> О нас </Nav.Link>
                            </Nav>

                            <AuthNavbar/>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </>
        )
    }
}