const ADMIN = "ADMIN"
const USER = "USER"
const UNAUTHORIZED_USER = "UNAUTHORIZED_USER"

const defaultState = {
    role: UNAUTHORIZED_USER,
    isAuth: false
}

export function logout() {
    console.log("IN logout");
    localStorage.removeItem('role');
    localStorage.setItem('email', UNAUTHORIZED_USER);
    localStorage.setItem('password', UNAUTHORIZED_USER);
    localStorage.setItem('role', UNAUTHORIZED_USER);
    return {
        isAuth: false
    }
}

export function setUser(action: string, email: string, password: string) {
    console.log("IN set user")
    localStorage.setItem('email', email);
    localStorage.setItem('password', password);
    switch (action) {
        case ADMIN:
            localStorage.setItem('role', ADMIN);
            return {}
        case USER:
            localStorage.setItem('role', USER);
            return {}
        default:
            localStorage.setItem('role', USER);
            return {isAuth: true}
    }
}