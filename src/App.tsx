import React from 'react';
import './App.css';
import Header from './layout/Header';
import Footer from './layout/Footer';
import {Route, Routes} from "react-router-dom"

import Home from './Pages/Home';
import Cinema from './Pages/Cinema';
import Contacts from './Pages/Contacts';
import About from './Pages/About';
import SessionPage from './Pages/SessionPage';
import MoviePage from './Pages/MoviePage';
import Registration from './components/Authorization/Registarion'
import Login from './components/Authorization/Login'
import PersonalAccount from './components/Authorization/PersonalAccount'
import Admin from './components/AdminAcc/Admin'
import AdminUserPage from './Pages/AdminUserPage';
import AdminMoviePage from './Pages/AdminMoviePage';
import AdminAuditoriumPage from './Pages/AdminAuditoriumPage'
import AdminSessionPage from './Pages/AdminSessionPage'
import AdminTheaterPage from './Pages/AdminTheaterPage'

function App() {

    return (
        <div>
            <Header/>
            <Footer/>
            <Routes>
                <Route path={'/'} element={<Home/>}/>
                <Route path={'/cinema'} element={<Cinema/>}/>
                <Route path={'/contacts'} element={<Contacts/>}/>
                <Route path={'/about'} element={<About/>}/>
                <Route path={'/cinema/movie:movieId/session:id'} element={<SessionPage/>}/>
                <Route path={'/cinema/movie:id'} element={<MoviePage/>}/>
                <Route path={'/admin'} element={<Admin/>}/>
                <Route path={'/admin/user:id'} element={<AdminUserPage/>}/>
                <Route path={'/admin/movie:id'} element={<AdminMoviePage/>}/>
                <Route path={'/admin/auditorium:id'} element={<AdminAuditoriumPage/>}/>
                <Route path={'/admin/session:id'} element={<AdminSessionPage/>}/>
                <Route path={'/admin/theater:id'} element={<AdminTheaterPage/>}/>
            </Routes>

            <div className="App">
                <Routes>
                    <Route path={'/registration'} element={<Registration/>}/>
                    <Route path={'/login'} element={<Login/>}/>
                    <Route path={'/account'} element={<PersonalAccount/>}/>
                </Routes>
            </div>
        </div>

    );
}

export default App;
