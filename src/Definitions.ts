export enum Status {
    FREE = 'free',
    OCCUPIED = 'occupied'
}

export type Seat = {
    x: number;
    y: number;
    price: number;
    status: Status;
}

export type Session = {
    id: number;
    datetime: object;
}

export type Theater = {
    id: number;
    name: string;
    address: string;
    sessions: Session[];
}

export type Movie = {
    maxPages: number;
    pageNum: number;
    theaters: Theater[];
}

export type SeatLayoutStatus = {
    session: number;
    seats: Seat[];
}

export type TicketOrder = {
    session: number;
    seats: Seat[];
}

export type RegisterOrder = {
    username: string;
    password: string;
}

export type ChangePasswordOrder = {
    username: string;
    oldPassword: string;
    newPassword: string;
}

export type AdminMovieOrder = {
    maxPages: number;
    pageNum: number;
    list: Movies[];
}

export type Movies = {
    id: number;
    name: string;
    description: string;
    previewUrl: string;
}

export type MovieOrder = {
    maxPages: number;
    pageNum: number;
    movies: Movies[];
}

export type AdminUserOrder = {
    maxPages: number;
    pageNum: number;
    list: User[];
}

export type User = {
    id: number;
    username: string;
    password: string;
    role: string;
}

export type AudSeat = {
    x: number;
    y: number;
    price: number;
}

export type Auditorium = {
    id: number;
    seats: AudSeat[];
    theaterId: number;
}

export type AdminAuditOrder = {
    maxPages: number;
    pageNum: number;
    list: Auditorium[];
}

export type AdminSession = {
    id: number;
    theaterId: number;
    movieId: number;
    auditoriumId: number;
    seats: Seat[];
    dateTime: object;
}

export type AdminSessionOrder = {
    maxPages: number;
    pageNum: number;
    list: AdminSession[];
}

export type AdminTheater = {
    id: number;
    name: string;
    address: string;
}

export type AdminTheaterOrder = {
    maxPages: number;
    pageNum: number;
    list: AdminTheater[];
}

