FROM node:12.22.9-alpine3.15 as build

COPY src /work/src
COPY public /work/public
COPY .env /work
COPY package.json /work
COPY package-lock.json /work
COPY tsconfig.json /work

WORKDIR /work

RUN npm install
RUN npm run build

FROM node:12.22.9-alpine3.15

COPY --from=build /work/build /work/build

RUN npm install -g serve

ENTRYPOINT ["serve", "-s", "build"]
